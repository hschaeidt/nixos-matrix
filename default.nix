{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.matrix;
in {
  options.matrix = {
    enable = mkEnableOption "nixos-matrix";

    enableRegistration = mkEnableOption "enable registration on this homeserver";

    ssl = mkOption {
      type = types.bool;
      default = true;
      description = ''
        Use let's encrypt for all certs.
      '';
    };

    fqdn = mkOption {
      type = types.str;
      example = "matrix.example.com";
      description = ''
        The fully qualified domain name of the matrix server.
        This domain name will also be used to open a minimal nginx reverse proxy
        for the matrix-synapse service.
      '';
    };

    turn = {
      enable = mkEnableOption "coturn as turn server";

      authSecret = mkOption {
        type = types.str;
        description = ''
          Matrix synapse will use this secret to authenticate against the coturn service.
        '';
      };
    };

    riot = {
      enable = mkEnableOption "riot web client";

      fqdn = mkOption {
        type = types.str;
        example = "chat.example.com";
        description = ''
          The fully qualified domain name of where the Riot web frontend will be deployed.
        '';
      };

      defaultHomeServerUrl = mkOption {
        type = types.str;
        example = "https://matrix.org/";
        default = "https://${cfg.fqdn}/";
        description = ''
          The default home server URL Riot should use.
        '';
      };

      defaultIdentityServerUrl = mkOption {
        type = types.str;
        example = "https://vector.im/";
        default = "https://${cfg.fqdn}/";
        description = ''
          The default identity server URL Riot should use.
        '';
      };

      disableCustomUrls = mkOption {
        type = types.bool;
        default = true;
        description = ''
          Wether or not to allow custom URLs in Riot.
        '';
      };

      disableGuests = mkOption {
        type = types.bool;
        default = true;
        description = ''
          Wether or not to allow guest logins through Riot.
          Guests must be enabled on the server too.
        '';
      };

      disableLoginLanguageSelector = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Wether or not to allow users to change the language for Riot.
        '';
      };

      disable3pidLogin = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Wether or not to allow third party ID logins.
          This can be an E-Mail address or phone number using an identity server.
        '';
      };

      brand = mkOption {
        type = types.str;
        default = "Riot";
      };
    };
  };

  config = mkIf cfg.enable {
    networking.firewall = {
      allowedTCPPorts = [
        80
        443
        # matrix-synapse federation port
        8448
      ] ++ optional cfg.turn.enable 3478;
      allowedUDPPorts = optional cfg.turn.enable 3478;
    };

    services = {
      nginx = {
        enable = true;
        virtualHosts = {
          # Reverse Proxy for matrix-synapse
          "${cfg.fqdn}" = {
            serverName = cfg.fqdn;
            forceSSL = cfg.ssl;
            enableACME = cfg.ssl;

            locations = {
              "/_matrix" = {
                proxyPass = "http://127.0.0.1:8008";
                extraConfig = ''
                  proxy_set_header X-Forwarded-For $remote_addr;
                '';
              };
            };
          };
        } // optionalAttrs cfg.riot.enable {
          # Riot web frontend configuration
          "${cfg.riot.fqdn}" = {
            serverName = cfg.riot.fqdn;
            forceSSL = cfg.ssl;
            enableACME = cfg.ssl;

            locations = {
              "/" = {
                root = pkgs.riot-web.override {
                  conf = ''
                    {
                      "default_hs_url": "${cfg.riot.defaultHomeServerUrl}",
                      "default_is_url": "${cfg.riot.defaultIdentityServerUrl}",
                      "disable_custom_urls": ${toString cfg.riot.disableCustomUrls},
                      "disable_guests": ${toString cfg.riot.disableGuests},
                      "disable_login_language_selector": ${toString cfg.riot.disableLoginLanguageSelector},
                      "disable_3pid_login": ${toString cfg.riot.disable3pidLogin},
                      "brand": "${cfg.riot.brand}",
                      "integrations_ui_url": "https://scalar.vector.im/",
                      "integrations_rest_url": "https://scalar.vector.im/api",
                      "integrations_jitsi_widget_url": "https://scalar.vector.im/api/widgets/jitsi.html",
                      "bug_report_endpoint_url": "https://riot.im/bugreports/submit",
                      "features": {
                          "feature_groups": "labs",
                          "feature_pinning": "labs"
                      },
                      "default_federate": true,
                      "welcomePageUrl": "home.html",
                      "default_theme": "dark",
                      "roomDirectory": {
                          "servers": [
                              "${cfg.fqdn}", "matrix.org"
                          ]
                      },
                      "welcomeUserId": "@riot-bot:matrix.org",
                      "piwik": {
                          "url": "http://localhost",
                          "whitelistedHSUrls": [],
                          "whitelistedISUrls": [],
                          "siteId": 1
                      },
                      "enable_presence_by_hs_url": {
                          "https://matrix.org": false
                      }
                    }
                  '';
                };
              };
            };
          };
        };
      };

      matrix-synapse = {
        enable = true;
        server_name = cfg.fqdn;
        enable_registration = cfg.enableRegistration;
        # turn configuration with coturn
        turn_uris = optionals cfg.turn.enable [
          "turn:${cfg.fqdn}:3478?transport=udp"
          "turn:${cfg.fqdn}:3478?transport=tcp"
        ];
        turn_shared_secret = mkIf cfg.turn.enable "${cfg.turn.authSecret}";
        turn_user_lifetime = "86400000";
        tls_certificate_path = mkIf cfg.ssl "/var/lib/acme/${cfg.fqdn}/fullchain.pem";
        tls_private_key_path = mkIf cfg.ssl "/var/lib/acme/${cfg.fqdn}/key.pem";
        # For simplicity do not reverse-proxy the federation port
        # See https://github.com/matrix-org/synapse#reverse-proxying-the-federation-port
        listeners = [{
          port = 8448;
          bind_address = "";
          type = "http";
          tls = true;
          x_forwarded = false;
          resources = [
            { names = ["federation"]; compress = false; }
          ];
        } {
          port = 8008;
          bind_address = "127.0.0.1";
          type = "http";
          tls = false;
          x_forwarded = true;
          resources = [
            { names = ["client" "webclient"]; compress = false; }
          ];
        }];
        #trusted_third_party_id_servers = [ "" ];
      };

      coturn = mkIf cfg.turn.enable {
        enable = true;
        lt-cred-mech = true;
        use-auth-secret = true;
        static-auth-secret = "${cfg.turn.authSecret}";
        realm = "${cfg.fqdn}";
        cert = mkIf cfg.ssl "/var/lib/acme/${cfg.fqdn}/fullchain.pem";
        pkey = mkIf cfg.ssl "/var/lib/acme/${cfg.fqdn}/key.pem";
      };
    };

    users.extraGroups.matrix-certs.members = mkIf cfg.ssl ([
      config.services.nginx.user "matrix-synapse"
    ] ++ optional cfg.turn.enable "turnserver");

    security.acme.certs = mkIf cfg.ssl {
      "${cfg.fqdn}" = {
        group = "matrix-certs";
        allowKeysForGroup = true;
        postRun = ''
          systemctl restart matrix-synapse
        '' + optionalString cfg.turn.enable ''
          systemctl restart coturn
        '';
      };
    };
  };
}
