{ pkgs, lib, ... }:

with lib;

{
  name = "riot";

  nodes.homeserver = { ... }: {
    imports = [ ./base.nix ];
    services.matrix-synapse.enable = lib.mkForce false;
    matrix.riot = {
      enable = true;
      fqdn = "riot.example.com";
    };
  };

  testScript = ''
    startAll;
    $homeserver->waitForUnit("multi-user.target");
    $homeserver->waitUntilSucceeds("curl -Lk --fail -H 'Host: riot.example.com' http://localhost/");
  '';
}
