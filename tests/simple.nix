{ pkgs, lib, ... }:

with lib;

{
  name = "simple";

  nodes.homeserver = { ... }: {
    imports = [ ./base.nix ];
  };

  testScript = ''
    startAll;
    $homeserver->waitForUnit("multi-user.target");
    $homeserver->waitUntilSucceeds("curl -Lk https://localhost:8448/");
    $homeserver->waitUntilSucceeds("curl -Lk http://localhost/_matrix/client/versions | grep versions");
  '';
}
