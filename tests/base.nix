{ ... }:

{
  imports = [ ../default.nix ];
  matrix = {
    enable = true;
    ssl = false;
    fqdn = "example.com";
  };
}
