{
  network.description = "matrix server";

  matrix =
    { config, pkgs, ... }:
    {
      imports = [
        ./../default.nix
      ];

      matrix = {
        enable = true;
        ssl = false;
        fqdn = "example.com";
      };
    };
}
